# Project 4: Brevet time calculator with Ajax

This calculator calculates ACP controle times, using the algorithm described here (https://rusa.org/pages/acp-brevet-control-times-calculator) and implemented here (https://rusa.org/octime_acp.html), using AJAX and Flask. It can be run by building a docker container, running it in the background, and then visiting the specified port. 

The project also contains a set of tests for testing the algorithm, which can be run by navigating to the brevets directory and typing "nosetests."

This project is for CIS 322 at University of Oregon.

Arden Butterfield, abutter2@uoregon.edu